import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Login } from './Login';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginClass: Login;
  httpService: LoginService;
  @Input() email: String;
  @Input() password: String;
  msg: string;


  constructor(private route: Router, httpService: LoginService) {
    this.loginClass = new Login();
    this.httpService = httpService;
  }

  ngOnInit() {
  }

  changeToRegistration() {
    this.route.navigate(['registration']);
  }

  getToken() {
    this.httpService.login(this.loginClass).subscribe(token => {
      this.httpService.email = this.email;
      // this.httpService.token = ;
      if (token.token !== null) {
        this.httpService.token = token.token;
        document.getElementById('emailText').innerHTML = String(this.email);
        document.getElementById('acc').style.width = 'auto';
        document.getElementById('acc').style.overflow = 'visible';
        this.route.navigate(['/reservations']);
      } else {
        this.msg = 'Niepoprawny login lub hasło';
      }

    });
  }

  logIn(email, password) {
    this.loginClass.email = email;
    this.loginClass.password = password;
    console.log(password + email);
    this.getToken();
  }
}
