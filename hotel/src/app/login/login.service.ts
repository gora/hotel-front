import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Login } from './Login';
import { map } from 'rxjs/operators';
import { Token } from './Token';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  token: String = '';
  url: String;
  email: String = '';

  constructor(private http: HttpClient) {
    this.url = 'https://promont.wroc.pl:8443/token';
  }

  login(login: Login): Observable<Token> {
    return this.http.post<Token>('https://promont.wroc.pl:8443/token', login);
  }
}
