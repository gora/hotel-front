import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  roomImage: String = '/assets/images/room.png';
  spaImage: String = '/assets/images/spa.png';
  sportImage: String = '/assets/images/sport.png';
  constructor() { }

  ngOnInit() {
  }

}
