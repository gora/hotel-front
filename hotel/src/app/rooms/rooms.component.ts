import { Component, OnInit } from '@angular/core';
import { Room } from './Room';
import { RoomsService } from './rooms.service';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {

  roomsList: Room[] = [];
  roomsOnPage: Room[] = [];
  typeList: Array<string>;
  httpService: RoomsService;
  show: boolean;

  constructor(httpService: RoomsService) {
    this.httpService = httpService;
    this.typeList = new Array;
  }

  ngOnInit() {
    this.getRooms();
  }

  getRooms() {
    this.httpService.getRooms().subscribe( rooms => {
      console.log(rooms);
      this.httpService.rooms = rooms;
      this.roomsList = rooms;
      this.roomsOnPage = rooms;
      this.getAllTypes();
    });
  }

  getAllTypes() {
    let is = false;
    for (const room of this.roomsList) {
      if (room.id_room !== 0 ) {
        for (const type of this.typeList) {
          if ( type === room.type ) {
          is = true;
          }
        }
        if ( !is ) {
          this.typeList.push(room.type);
        }
        is = false;
      } else {
        this.typeList.push(room.type);
      }
    }
    console.log(this.typeList);
  }

  onSelect(type) {
    console.log(type);
    if ( type !== 'Wszystkie') {
      this.roomsOnPage = [];
      console.log(this.roomsOnPage);
      for (const room of this.roomsList) {
        if ( room.type === type ) {
          this.roomsOnPage.push(room);
        }
      }
    } else {
      this.roomsOnPage = this.roomsList;
    }

    }

  checkAvail() {
    this.show = true;
  }
}
