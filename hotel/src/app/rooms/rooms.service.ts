import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';
import { Room } from './Room';
import { AvailRoom } from '../room-availability/AvailRoom';
@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  rooms: Room[] = [];
  url: String;

  constructor(private http: HttpClient) { }

  getRooms(): Observable<Array<Room>> {
    this.url = 'http://promont.wroc.pl:1515/api/rooms/getAllRooms';
    return this.http.get<Array<Room>>('https://promont.wroc.pl:8443/api/rooms/getAllRooms');
  }


  checkAvailability(checkRoom: AvailRoom): Observable<Array<Room>> {
    return this.http.post<Array<Room>>('https://promont.wroc.pl:8443/api/rooms/checkAvailability', checkRoom);
  }
}
