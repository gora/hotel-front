export class Room {
  public id_room: Number;
  public type: string;
  public long_description: string;
  public price: Number;
  public is_open: Boolean;
}
