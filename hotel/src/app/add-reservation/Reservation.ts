export class Reservation {
  public reservationId: Number;
  public certionDate: Date;
  public arrival: String;
  public departure: String;
  public roomId: Number;
  public clientEmial: String;
  public paymentDate: Date;
  public cancelationDate: Date;
  public payment: Number;
}
