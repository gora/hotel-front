import { Router } from '@angular/router';
import { LoginService } from './../login/login.service';
import { Room } from './../rooms/Room';
import { Component, OnInit, Input } from '@angular/core';
import { RoomsService } from '../rooms/rooms.service';
import { Login } from '../login/Login';
import { Reservation } from './Reservation';
import { ReservationService } from './reservation.service';

@Component({
  selector: 'app-add-reservation',
  templateUrl: './add-reservation.component.html',
  styleUrls: ['./add-reservation.component.css']
})
export class AddReservationComponent implements OnInit {
  roomSer: RoomsService;
  httpLogin: LoginService;
  reservationHttp: ReservationService;
  rooms: Room[] = [];
  certionDate: Date;
  email: String;
  reservation: Reservation;
  showAdd: boolean;
  payShow: boolean;
  pay: string;
  today: Date;

  @Input() arrival: String;
  @Input() departure: String;
  @Input() roomNr: Number;
  msg: string;

  // show


  constructor(roomSer: RoomsService, httpLogin: LoginService, reservationHttp: ReservationService, private router: Router) {
    this.roomSer = roomSer;
    this.httpLogin = httpLogin;
    this.reservationHttp = reservationHttp;
    this.reservation = new Reservation();
    this.msg = '';
   }

  ngOnInit() {
    if (this.httpLogin.token !== '') {
      this.getRooms();
      this.showAdd = true;
      this.payShow = false;
      this.email = this.httpLogin.email;
    } else {
      this.router.navigate(['/login']);
    }

  }

  getRooms() {
    this.roomSer.getRooms().subscribe( rooms => {
      this.rooms = rooms;
    });
  }

  addReservation() {

    this.reservationHttp.addReservation(this.reservation).subscribe(price => {
      if (price.text === 'Nie mozna rozpoczac rezerwacji, ktora juz sie odbyla') {
        this.msg = 'Niepoprawnie ustawiono daty.';
      } else {
        this.showAdd = false;
        this.payShow = true;
        this.pay = price.text;
      }

    });
  }

  createReservation(arrival, departure, roomNr) {
    this.today = new Date();
    if (arrival < departure) {
      this.msg = '';
      this.reservation.arrival = arrival;
      this.reservation.departure = departure;
      this.reservation.roomId = roomNr;
      this.reservation.certionDate = new Date();
      this.reservation.clientEmial = this.email;
      this.addReservation();
    } else {
      this.msg = 'Niepoprawnie ustawiono daty.';
    }

  }
  goToRooms() {
    this.router.navigate(['/rooms']);
  }

}
