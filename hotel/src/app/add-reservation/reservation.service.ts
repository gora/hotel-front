import { LoginService } from './../login/login.service';
import { Injectable } from '@angular/core';
import { Reservation } from './Reservation';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';
import { Obj } from './Obj';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  login: LoginService;
  constructor(private http: HttpClient, login: LoginService) {
    this.login = login;
  }

  addReservation(reservation: Reservation): Observable<Obj> {
    console.log(this.login.token);
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Token ' + this.login.token});
    console.log(headersForInvitesAPI);
    return this.http.post<Obj>('https://promont.wroc.pl:8443/rest/reservations/addReservation',
    reservation, {headers: headersForInvitesAPI});
  }

  getReservation(): Observable<Array<Reservation>> {
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Token ' + this.login.token});
    return this.http.get<Array<Reservation>>('https://promont.wroc.pl:8443/rest/reservations/getReservations',
    {headers: headersForInvitesAPI});
  }

  cancelReservation(reservation: Reservation): Observable<Obj> {
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Token ' + this.login.token});
    return this.http.post<Obj>('https://promont.wroc.pl:8443/rest/reservations/cancelReservation',
    reservation, {headers: headersForInvitesAPI});
  }
}
