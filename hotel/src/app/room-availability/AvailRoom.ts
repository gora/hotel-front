export class AvailRoom {
  public arrival: string;
  public departure: string;
  public roomType: string;
}
