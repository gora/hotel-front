import { AvailRoom } from './AvailRoom';
import { RoomsService } from './../rooms/rooms.service';
import { Room } from './../rooms/Room';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-room-availability',
  templateUrl: './room-availability.component.html',
  styleUrls: ['./room-availability.component.css']
})
export class RoomAvailabilityComponent implements OnInit {

  roomsList: Room[] = [];
  roomsOnPage: Room[] = [];
  checkRoom: AvailRoom;
  availRooms: Room[] = [];
  typeList: Array<string>;
  httpService: RoomsService;
  show: boolean;
  @Input() departure: string;
  @Input() arrival: string;
  chosenType: string;
  noAvaRooms: boolean;
  @Input() modelType: string;
  msg: string;
  today: Date;
  todString: string;

  constructor(httpService: RoomsService) {
    this.httpService = httpService;
    this.typeList = new Array;
    this.checkRoom = new AvailRoom();
    this.noAvaRooms = false;
    this.msg = '';
  }

  ngOnInit() {
    this.getRooms();
  }

  getRooms() {
    this.httpService.getRooms().subscribe(rooms => {
      console.log(rooms);
      this.httpService.rooms = rooms;
      this.roomsList = rooms;
      this.roomsOnPage = rooms;
      this.getAllTypes();
    });
  }

  getAllTypes() {
    let is = false;
    for (const room of this.roomsList) {
      if (room.id_room !== 0) {
        for (const type of this.typeList) {
          if (type === room.type) {
            is = true;
          }
        }
        if (!is) {
          this.typeList.push(room.type);
        }
        is = false;
      } else {
        this.typeList.push(room.type);
      }
    }
    console.log(this.typeList);
    this.chosenType = this.typeList[0];
  }

  onSelect(type) {
    console.log(type);
    this.chosenType = type;
  }

  onDateD(event) {
    console.log(event);
    this.departure = event;
  }

  onDateA(event) {
    console.log(event);
    this.arrival = event;
  }



  checkAvail2() {
    this.httpService.checkAvailability(this.checkRoom).subscribe(rooms => {
      if (rooms.length > 0) {
        this.noAvaRooms = false;
        this.roomsOnPage = rooms;
        console.log(rooms);
      } else {
        console.log(rooms);
        this.roomsOnPage = [];
        this.noAvaRooms = true;
      }
    });
  }

  checkAvail(arrival, departure) {
    this.today = new Date();
    const mon = this.today.getMonth() + 1;
    this.todString = this.today.getFullYear() + '-' +  mon  + '-' + this.today.getDate();
    console.log(arrival + 'blab la' + this.todString);
    if (arrival > this.todString) {
      if (arrival < departure) {
        this.msg = '';
        this.checkRoom.arrival = arrival;
        this.checkRoom.departure = departure;
        console.log(this.checkRoom);
        this.checkRoom.roomType = this.chosenType;
        this.checkAvail2();
      } else {
        this.roomsOnPage = [];
        this.msg = 'Niepoprawnie ustawiono daty.';
      }
    } else {
      this.roomsOnPage = [];
      this.msg = 'Niepoprawnie ustawiono daty.';
    }

  }
}
