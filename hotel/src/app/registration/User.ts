import { Adres } from './Adres';
export class User {
  public email: String;
  public firstName: String;
  public lastName: String;
  public identyficationCard: String;
  public password: String;
  public phone: String;
  public address: Adres;
}
