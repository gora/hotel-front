export class Adres {
  public apartamentNr: String;
  public city: String;
  public country: String;
  public houseNr: String;
  public postalCode: String;
  public street: String;

}
