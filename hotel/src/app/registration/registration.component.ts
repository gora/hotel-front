import { Adres } from './Adres';
import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { RegistrationService } from './registration.service';
import { User } from './User';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  httpService: RegistrationService;
  user: User;
  adres: Adres;

  @Input() firstName: String;
  @Input() lastName: String;
  @Input() tel: String;
  @Input() card: String;
  @Input() email: String;
  @Input() password: String;

  @Input() street: String;
  @Input() hnr: String;
  @Input() anr: String;
  @Input() city: String;
  @Input() posCode: String;
  @Input() coun: String;

  @Input() rodo: boolean;
  @Input() cookie: boolean;
  msg: string;



  constructor(httpService: RegistrationService, private route: Router) {
    this.httpService = httpService;
    this.user = new User;
    this.adres = new Adres;
    this.rodo = false;
    this.cookie = false;
    this.msg = '';
  }

  ngOnInit() {
  }

  // addAndGetIdAdres (adres) {
  //   this.httpService.addUserAdress(adres).subscribe( id => {
  //     this.user.idAddress = id;
  //     console.log('add' + id);
  //     this.addUser(this.user);
  //     console.log(this.adres);
  //     console.log(this.user);
  //     this.route.navigate(['login']);
  //   });
  // }

  addUser(user) {
    this.httpService.addUser(user).subscribe(id => {
      console.log(id);
      this.route.navigate(['login']);
    });
  }


  addObject(firstName, lastName, card, tel, email, password, street, hnr, anr, city, posCode, coun, rodo, cookie) {
    // user
    // console.log(firstName);
    // console.log(rodo + "   " + cookie);

    if (rodo === true && cookie === true) {
      // tslint:disable-next-line:max-line-length
      //  && !(firstName === null && lastName === null && card === null && email === null && password === null && street === null && hnr === null && city === null && posCode === null && coun === null)
      this.adres.street = street;
      this.adres.houseNr = hnr;
      this.adres.apartamentNr = anr;
      this.adres.city = city;
      this.adres.postalCode = posCode;
      this.adres.country = coun;
      this.user.firstName = firstName;
      this.user.lastName = lastName;
      this.user.identyficationCard = card;
      this.user.phone = tel;
      this.user.email = email;
      this.user.password = password;
      this.user.address = this.adres;
      console.log(this.user + '\n' + this.adres);
      this.addUser(this.user);
    } else {
      this.msg = 'Nie zaakceptowano warunków';
    }


  }

  closeModel() {
    this.route.navigate(['login']);
  }


}
