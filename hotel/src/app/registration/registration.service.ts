import { User } from './User';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {
  url: String;
  constructor(private http: HttpClient) {
    this.url = 'http://promont.wroc.pl:1515/registerClient';
  }

  addUser(user: User): Observable<User> {
    return this.http.post<User>('https://promont.wroc.pl:8443/registerClient', user);
  }
}
