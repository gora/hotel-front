import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { LoginService } from '../login/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input() email: String;
  isToken: boolean;

  constructor(private httpLogin: LoginService, private router: Router) {
    this.email = this.httpLogin.email;
    this.isToken = false;
    //
  }

  ngOnInit() {
    this.isToken = false;
    this.email = this.httpLogin.email;
  }

  logOut() {
    this.httpLogin.token = '';
    this.router.navigate(['/login']);
    document.getElementById('acc').style.width = '0vw';
    document.getElementById('acc').style.overflow = 'hidden';
    this.checkToken();
  }

  checkToken() {
    if (this.httpLogin.token !== '') {
      this.isToken = true;
      this.email = this.httpLogin.email;
    } else {
      this.isToken = false;
    }
    return this.isToken;
  }


}
