import { RoomAvailabilityComponent } from './room-availability/room-availability.component';
import { RegistrationComponent } from './registration/registration.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RoomsComponent } from './rooms/rooms.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ReservationsComponent } from './reservations/reservations.component';
import { AddReservationComponent } from './add-reservation/add-reservation.component';
import { YourAccountComponent } from './your-account/your-account.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, pathMatch: 'full'},
  { path: 'registration', component: RegistrationComponent},
  { path: 'rooms', component: RoomsComponent},
  { path: 'home', component: HomePageComponent},
  { path: 'reservations', component: ReservationsComponent},
  { path: 'addReservation', component: AddReservationComponent},
  { path: 'yourAcconut', component: YourAccountComponent},
  { path: 'availability', component: RoomAvailabilityComponent},
  { path: '**', component: HomePageComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  declarations: []
})
export class AppRoutingModule { }
