import { Component, OnInit } from '@angular/core';
import { ReservationService } from '../add-reservation/reservation.service';
import { Reservation } from '../add-reservation/Reservation';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css']
})
export class ReservationsComponent implements OnInit {

  reservationShow: Reservation[] = [];
  resAnuluj: Reservation;
  constructor(private reservationHttp: ReservationService, private httpLogin: LoginService, private router: Router) {
    this.resAnuluj = new Reservation();
  }

  ngOnInit() {
    if (this.httpLogin.token !== '') {
      this.getReserv();
    } else {
      this.router.navigate(['/login']);
    }
  }

  getReserv() {
    this.reservationHttp.getReservation().subscribe(res => {
      this.reservationShow = res;
    });
  }

  cancel() {
    this.reservationHttp.cancelReservation(this.resAnuluj).subscribe(ans => {
      console.log(ans);
      this.getReserv();
    });
  }

  cancelPop(r) {
    this.resAnuluj = r;
  }
}
