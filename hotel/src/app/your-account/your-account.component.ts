import { Adres } from './../registration/Adres';
import { YourAccountService } from './your-account.service';
import { Component, OnInit, Input } from '@angular/core';
import { User } from '../registration/User';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';
import { ChangePass } from './ChangePass';

@Component({
  selector: 'app-your-account',
  templateUrl: './your-account.component.html',
  styleUrls: ['./your-account.component.css']
})
export class YourAccountComponent implements OnInit {

  editBox: boolean;
  userBox: boolean;
  editPasBox: boolean;
  user: User;
  userEdit: User;
  adres: Adres;

  // edit user
  @Input() firstName: String;
  @Input() lastName: String;
  @Input() tel: String;
  @Input() card: String;
  @Input() email: String;
  @Input() password: String;

  @Input() street: String;
  @Input() hnr: String;
  @Input() anr: String;
  @Input() city: String;
  @Input() posCode: String;
  @Input() coun: String;

  @Input() login: String;
  @Input() oldPass: String;
  @Input() newPass: String;
  @Input() newPassCon: String;
  confiPass: boolean;
  changePass: ChangePass;

  constructor(private http: YourAccountService, private httpLogin: LoginService, private router: Router) {
    this.user = new User();
    this.userEdit = new User();
    this.adres = new Adres();
    this.changePass = new ChangePass();
    this.confiPass = false;
  }

  ngOnInit() {
    if (this.httpLogin.token !== '') {
      this.editBox = false;
      this.userBox = true;
      this.editPasBox = false;
      this.getUser();
    } else {
      this.router.navigate(['/login']);
    }

  }

  editUser(firstName, lastName, card, tel, email, password, street, hnr, anr, city, posCode, coun) {
    this.editBox = true;
    this.userBox = false;
    this.editPasBox = false;

    this.street = street;
    this.hnr = hnr;
    this.anr = anr;
    this.city = city;
    this.posCode = posCode;
    this.coun = coun;

    this.firstName = firstName;
    this.lastName = lastName;
    this.card = card;
    this.tel = tel;
    this.email = email;
    // this.userEdit.password = password;
    this.adres = this.adres;
  }

  editAcc(firstName, lastName, card, tel, email, password, street, hnr, anr, city, posCode, coun) {
    this.editBox = true;
    this.editPasBox = false;
    this.userBox = false;
    this.adres.street = street;
    this.adres.houseNr = hnr;
    this.adres.apartamentNr = anr;
    this.adres.city = city;
    this.adres.postalCode = posCode;
    this.adres.country = coun;

    this.userEdit.firstName = firstName;
    this.userEdit.lastName = lastName;
    this.userEdit.identyficationCard = card;
    this.userEdit.phone = tel;
    this.userEdit.email = email;
    this.userEdit.password = password;
    this.userEdit.address = this.adres;

  }

  editPop() {
    this.putUser();
  }

  putUser() {
    this.http.editAccount(this.userEdit).subscribe(ans => {
      this.user = this.userEdit;
      this.userBox = true;
      this.editBox = false;
      this.editPasBox = false;
    });
  }

  deletePop() {
    this.http.deleteAccount().subscribe(ans => {
      this.httpLogin.token = '';
      this.router.navigate(['/login']);
      document.getElementById('acc').style.width = '0vw';
      document.getElementById('acc').style.overflow = 'hidden';
    });
  }

  getUser() {
    this.http.getAccount().subscribe(user => {
      this.user = user;
      // console.log(user);
      this.showUserBox();
    });
  }

  showUserBox() {
    this.userBox = true;
    this.editBox = false;
    this.editPasBox = false;
  }

  changePassword() {
    this.http.editPass(this.changePass).subscribe(ans => {
      console.log(ans);
      if (ans.text === 'Zaktualizowano') {
        this.httpLogin.token = '';
        this.router.navigate(['/login']);
        document.getElementById('acc').style.width = '0vw';
        document.getElementById('acc').style.overflow = 'hidden';
      } else {
        this.confiPass = true;
      }
    });
  }

  editPass(login) {
    this.editBox = false;
    this.userBox = false;
    this.editPasBox = true;
    this.login = login;
  }

  changePop(email, oldPass, newPass, newPassCon) {
    if (newPass === newPassCon) {
      this.confiPass = false;
      this.changePass.email = email;
      this.changePass.newPass = newPass;
      this.changePass.oldPass = oldPass;
      this.changePassword();
    } else {
      this.confiPass = true;
    }

  }
}
