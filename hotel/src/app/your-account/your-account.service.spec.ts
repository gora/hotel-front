import { TestBed, inject } from '@angular/core/testing';

import { YourAccountService } from './your-account.service';

describe('YourAccountService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [YourAccountService]
    });
  });

  it('should be created', inject([YourAccountService], (service: YourAccountService) => {
    expect(service).toBeTruthy();
  }));
});
