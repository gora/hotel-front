import { ChangePass } from './ChangePass';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from '../../../node_modules/rxjs';
import { LoginService } from '../login/login.service';
import { User } from '../registration/User';
import { Obj } from '../add-reservation/Obj';

@Injectable({
  providedIn: 'root'
})
export class YourAccountService {

  constructor(private httpLogin: LoginService, private http: HttpClient) { }

  deleteAccount(): Observable<any> {
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Token ' + this.httpLogin.token});
    return this.http.delete('https://promont.wroc.pl:8443/rest/userDetails/deleteUser', {headers: headersForInvitesAPI});
  }

  getAccount(): Observable<User> {
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Token ' + this.httpLogin.token});
    return this.http.get<User>('https://promont.wroc.pl:8443/rest/userDetails/getUserDetails', {headers: headersForInvitesAPI});
  }

  editAccount(user: User): Observable<User> {
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Token ' + this.httpLogin.token});
    return this.http.post<User>('https://promont.wroc.pl:8443/rest/userDetails/updateUser', user, {headers: headersForInvitesAPI});
  }

  editPass(pass: ChangePass): Observable<Obj> {
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Token ' + this.httpLogin.token});
    return this.http.post<Obj>('https://promont.wroc.pl:8443/rest/userDetails/updatePassword', pass, {headers: headersForInvitesAPI});
  }
}
