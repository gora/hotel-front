export class ChangePass {
    public email: String;
    public oldPass: String;
    public newPass: String;
}